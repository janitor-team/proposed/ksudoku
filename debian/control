Source: ksudoku
Section: games
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Daniel Schepler <schepler@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Modestas Vainius <modax@debian.org>,
           George Kiagiadakis <gkiagiad@csd.uoc.gr>,
           Eshat Cakar <info@eshat.de>,
           Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
           Maximiliano Curia <maxy@debian.org>,
Build-Depends: cmake (>= 3.5~),
               debhelper-compat (= 12),
               extra-cmake-modules (>= 5.46.0~),
               gettext,
               libgl1-mesa-dev | libgl-dev,
               libglu1-mesa-dev,
               libkf5archive-dev (>= 5.46.0~),
               libkf5config-dev (>= 5.46.0~),
               libkf5configwidgets-dev (>= 5.46.0~),
               libkf5coreaddons-dev (>= 5.46.0~),
               libkf5crash-dev (>= 5.46.0~),
               libkf5doctools-dev (>= 5.46.0~),
               libkf5guiaddons-dev (>= 5.46.0~),
               libkf5i18n-dev (>= 5.46.0~),
               libkf5iconthemes-dev (>= 5.46.0~),
               libkf5jobwidgets-dev (>= 5.46.0~),
               libkf5kdegames-dev (>= 4.9.0~),
               libkf5kio-dev (>= 5.46.0~),
               libkf5widgetsaddons-dev (>= 5.46.0~),
               libkf5xmlgui-dev (>= 5.46.0~),
               libqt5opengl5-dev (>= 5.11.0~),
               libqt5svg5-dev (>= 5.11.0~),
               pkg-kde-tools (>= 0.14),
               qtbase5-dev (>= 5.11.0~),
               qtdeclarative5-dev (>= 5.11.0~),
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/ksudoku
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/ksudoku.git

Package: ksudoku
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: ${kde-l10n:all}
Replaces: ${kde-l10n:all}
Recommends: khelpcenter
Description: Sudoku puzzle game and solver
 KSudoku is a Sudoku game and solver, supporting a range of 2D and 3D Sudoku
 variants.  In addition to playing Sudoku, it can print Sudoku puzzle sheets
 and find the solution to any Sudoku puzzle.
 .
 This package is part of the KDE games module.
